Spree::Admin::ReportsController.class_eval do
	
	ADVANCED_REPORTS = {
                      :out_stock => {
                        	:name => "Stock Report", 
                        	:description => "The report show all products that in a range of dates ran out of stock"
                      	}
	                    }
	Spree::Admin::ReportsController::available_reports.merge!(ADVANCED_REPORTS)

	def out_stock
    @filterrific = initialize_filterrific(
      Spree::StockMovement,
      params[:filterrific],
      select_options: {},
      persistence_id: 'shared_key'
    ) or return
		@stock_moves = @filterrific.find().with_out_stock()
    respond_to do |format|
      format.html { render :template => "spree/admin/reports/out_stock" }
    end		
	end
end