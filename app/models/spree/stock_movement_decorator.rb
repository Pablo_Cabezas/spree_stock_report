Spree::StockMovement.class_eval do
  filterrific(
    default_filter_params: { },
    available_filters: [
      :with_out_stock, :by_product_sku, :by_date_start, :by_date_stop
    ]
  )
  
  scope :with_out_stock, -> () {
  	where('quantity <= 0')
  }

  scope :by_product_sku, -> (product_sku) {
    joins( stock_item: :variant).where( spree_variants: { :sku =>  product_sku })
  }

  scope :by_date_start, -> (date_start) {
    where("created_at::date >= '#{ date_start }'")
  }

  scope :by_date_stop, -> (date_stop) {
    where("created_at::date <= '#{ date_stop }'")
  }
end