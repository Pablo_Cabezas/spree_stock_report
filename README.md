SpreeStockReport
================

Introduction goes here.

## Installation

1. Add this extension to your Gemfile with this line:
  ```ruby
  gem 'spree_stock_report', :git => "https://Pablo_Cabezas@bitbucket.org/Pablo_Cabezas/spree_stock_report.git", :branch => 'master'
  ```

2. Install the gem using Bundler:
  ```ruby
  bundle install
  ```

3. Restart your server

  If your server was running, restart it so that it can find the assets properly.

## Contributing

If you'd like to contribute, please take a look at the
[instructions](CONTRIBUTING.md) for installing dependencies and crafting a good
pull request.

Copyright (c) 2017 [pablo cabezas], released under the New BSD License
