Spree::Core::Engine.add_routes do
  # Add your extension routes here
	match '/admin/reports/stocks' => 'admin/reports#out_stock', :via => [:get, :post], :as => 'out_stock_admin_reports'
end