module Reports::OutStock
  module_function

  def get_data(params)
    Spree::StockItem.includes(variant: :product).where('count_on_hand <= 0')
  end  

  def clean_params(params)
    params[:q] ||= {}
    if params[:q][:completed_at_gt].blank?
      params[:q][:completed_at_gt] = Time.zone.now.beginning_of_month
    else
      params[:q][:completed_at_gt] = Time.zone.parse(params[:q][:completed_at_gt]).beginning_of_day rescue Time.zone.now.beginning_of_month
    end
    if params[:q][:completed_at_lt].blank?
      params[:q][:completed_at_lt] = Time.zone.now.end_of_month
    else
      params[:q][:completed_at_lt] = Time.zone.parse(params[:q][:completed_at_lt]).end_of_day rescue Time.zone.now.end_of_month
    end
    return params
  end
end